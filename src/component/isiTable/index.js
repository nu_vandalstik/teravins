import React, { useEffect, useState } from "react";
import axios from "axios";

function Index(props) {
  const { name, email, mobile, id } = props;
  const [object, setObject] = useState({
    _id: "",
    address: "",
    birthdate: "",
    email: "",
    mobile: "",
    name: "",
  });

  const detailData = async (e) => {
    axios
      .get(`http://127.0.0.1:4000/v1/user/${id}`)
      .then(function (response) {
        // handle success
        let dat = response.data.data;
        setObject({
          _id: dat._id,
          address: dat.address,
          birthdate: dat.birthdate,
          email: dat.email,
          mobile: dat.mobile,
          name: dat.name,
        });
        //   console.log(response.data.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  };



  useEffect(() => {
    console.log(object);
    document.getElementById('address').innerHTML=object._id
    document.getElementById('objectid')


    // detailData()
  }, [object])

  return (
    <tr>
      <th scope="row">{id || "Empty"}</th>
      <td>{name || "Empty"}</td>
      <td>{email || "Empty"}</td>
      <td>{mobile || "Empty"}</td>
      <td>
        <button
          type="button"
          className="btn btn-primary ms-1"
          data-bs-toggle="modal"
          data-bs-target="#detail"
          onClick={detailData}
        >
          Detail
        </button>

        <div
          className="modal fade"
          id="detail"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Detail Data
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <div className="container">
                  <div className="row align-items-start">
                    <div className="col" id="objectid">ID : {object._id}</div>
                    <div className="col" id="address">{object.address}</div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>

        <button
          type="button"
          className="btn btn-warning ms-1"
          data-bs-toggle="modal"
          data-bs-target="#edit"
        >
          Edit
        </button>

        <div
          className="modal fade"
          id="edit"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Modal title
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">...</div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
        <button
          type="button"
          className="btn btn-danger ms-1"
          data-bs-toggle="modal"
          data-bs-target="#delete"
        >
          Delete
        </button>

        <div
          className="modal fade"
          id="delete"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Modal title
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">...</div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>
  );
}

export default Index;
