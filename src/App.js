import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import * as yup from "yup";
import IsiTable from './component/isiTable'



function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(99);
const [conditional, setConditional] = useState('')



  const [upData, setupData] = useState({
    name : '',
    email: '',
    mobile: '',
    birthdate : '',
    address : '',
  });


  const handleChange = (e) => {
    e.persist();
    setupData((upData) => ({
      ...upData,
      [e.target.name]: e.target.value,
     
    }
    
    
    ));
    // console.log(e.target.value)
  };
  
  
 const handlePerPage = (event)=> {
    let value = event.target.value;
    setPerPage(value);
}

const handleSubmit = (e)=>{
  // console.log(upData)
  let data = yup.object().shape({
    name: yup.string().required(),
    email: yup.string().email().required(),
    mobile: yup.number().required().positive().integer(),
    birthdate : yup.string().required(),
    address : yup.string().required()
  })

  data.validate({
    name : upData.name,
    email: upData.email,
    mobile: upData.mobile,
    birthdate : upData.birthdate,
    address : upData.address,
  }).then( function (valid) {
    // console.log(valid)
    if(valid){
      setupData({
        name : upData.name,
        email: upData.email,
        mobile: upData.mobile,
        birthdate : upData.birthdate,
        address : upData.address,
      })

      axios.post(`http://127.0.0.1:4000/v1/user`, {
        name : valid.name,
        email: valid.email,
        mobile: valid.mobile,
        birthdate : valid.birthdate,
        address : valid.address,
      })

    }

alert('Wait 3 Second !')
setTimeout(() => {
  window.location.reload()
}, 3000);
  })
  .catch(function (err) {
    setConditional( err.errors)
    // console.log(err)
  })
}


  const getData = async () => {     
    axios
      .get(`http://127.0.0.1:4000/v1/user?page=${page}&perPage=${perPage}`)
      .then(function (response) {
        // handle success
        // console.log(response.data.data);
        setIsLoaded(true);
        setItems(response.data.data);

        // console.log(items);
      })
      .catch(function (error) {
        // handle error
        // console.log(error);

        setIsLoaded(true);
        setError(error);
      });
  };

  useEffect(() => {
    getData();
  }, []);



  // console.log(isLoaded);
  return (




    
    <div className="wrapper">


      
      <div className="container mt-2">
        <div className="row align-items-start">
          <div className="col">
            <h4>List Employee</h4>
          </div>
          <div className="col">
            <form className="d-flex">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button className="btn btn-outline-success" type="submit">
                Search
              </button>
            </form>
          </div>
          <div className="col">
            {/* <!-- Button trigger modal --> */}
            <button
              type="button"
              className="btn btn-primary"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              ADD
            </button>

            {/* <!-- Modal --> */}
            <div
              className="modal fade"
              id="exampleModal"
              tabIndex="-1"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                     {conditional || "Add Data"} 
                    </h5>
                    
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    {/* dadsad */}

                    <div className="row g-3 align-items-center mb-3">
                      <div className="col-auto">
                        <label
                          htmlFor="inputPassword6"
                          className="col-form-label"
                        >
                          Name
                        </label>
                      </div>
                      <div className="col-auto ms-5">
                        <input type="text" 
                        id="text" 
                        className="form-control"
                        name="name"
                        id="name"
                        placeholder="Name"
                        value={upData.name || ''}
                        onChange={handleChange}
                        />
                      </div>
                    </div>

                    <div className="row g-3 align-items-center mb-3">
                      <div className="col-auto">
                        <label
                          htmlFor="inputPassword6"
                          className="col-form-label"
                        >
                          Email
                        </label>
                      </div>
                      <div className="col-auto ms-5">
                        <input
                          type="email"
                          id="text"
                          className="form-control"
                          name="email"
                          id="email"
                          placeholder="Email"
                          value={upData.email || ''}
                          onChange={handleChange}
                        />
                      </div>
                    </div>

                    <div className="row g-3 align-items-center mb-3">
                      <div className="col-auto">
                        <label
                          htmlFor="inputPassword6"
                          className="col-form-label"
                        >
                          Mobile
                        </label>
                      </div>
                      <div className="col-auto ms-5">
                        <input
                          type="number"
                          id="text"
                          className="form-control"
                          name="mobile"
                          id="mobile"
                          placeholder="mobile"
                          value={upData.mobile || ''}
                          onChange={handleChange}
                        />
                      </div>
                    </div>

                    <div className="row g-3 align-items-center mb-3">
                      <div className="col-auto">
                        <label
                          htmlFor="inputPassword6"
                          className="col-form-label"
                        >
                          Birthdate
                        </label>
                      </div>
                      <div className="col-auto ms-5">
                        <input type="date" id="text" className="form-control"
                                           name="birthdate"
                                           id="birthdate"
                                           placeholder="BirthDate"
                                           value={upData.birthdate || ''}
                                           onChange={handleChange}
                        
                        
                        />
                      </div>
                    </div>

                    <div className="row g-3 align-items-center mb-3">
                      <div className="col-auto">
                        <label
                          htmlFor="inputPassword6"
                          className="col-form-label"
                        >
                          Address
                        </label>
                      </div>
                      <div className="col-auto ms-5">
                        <input type="text" id="text" className="form-control" 
                                           name="address"
                                           id="address"
                                           placeholder="Your Address"
                                           value={upData.address || ''}
                                           onChange={handleChange}
                        
                        
                        />
                      </div>
                    </div>

                    {/* asdsadsadsa */}
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-bs-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="button" className="btn btn-primary" onClick={handleSubmit}>
                      Save changes
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Mobile</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>





            
            {items.map((data) => {
              return (
                // isi table
                <IsiTable key={data._id}
                 name={data.name} 
              email={data.email} 
              mobile={data.mobile} 
              id={data._id}
                
                ></IsiTable>
              );
            })}

            
          </tbody>
        </table>
      </div>

      <div className="container">
        <p style={{ marginRight: 20 }}>Show</p>
        <ul className="pagination">
          <select
            className=""
            aria-label=".form-select-lg example"
            style={{ marginRight: 20 }}
        onChange={handlePerPage}
        value={perPage}
          >
            <option defaultValue>10</option>
            <option value="3">20</option>
            <option value="5">30</option>
            <option value="3">40</option>
          </select>
          <li className="page-item">
            <a className="page-link" href="#">
              Previous
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              1
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              2
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="#">
              Next
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default App;
